import serial
import time
import binascii

# Gateway IDs
DEV_UI = ""
APP_UI = ""
APP_KEY = ""

# Serial propriety
SERIAL_PORT = "/dev/ttyUSB0"
BAUDRATE = 57600

# Config lora
config = [
    "mac set deveui " + DEV_UI,
    "mac set appeui " + APP_UI,
    "mac set appkey " + APP_KEY,
    "mac set dr 0",
    "mac set adr on",
    "mac set bat 127",
    "mac set retx 2",
    "mac set linkchk 100",
    "mac set rxdelay1 1000",
    "mac set ar on",
    "mac get rx2 868",
    "mac set rx2 3 869525000",

    # set DC to 50%
    "mac set ch dcycle 0 1",
    "mac set ch dcycle 1 1",
    "mac set ch dcycle 2 1",
    "mac save",
]


class Lora():
    def __init__(self):

        print("init serial port ...")
        self.ser = serial.Serial(port=SERIAL_PORT, baudrate=BAUDRATE)

        print("serial port initialized ")

    def get_info_device(self):
        """
        return microchip RN2483 information
        """

        self.exec_serial_cmd("sys get ver")
        self.exec_serial_cmd("sys get vdd")
        self.exec_serial_cmd("sys get hweui")

    def get_keys(self):
        """
        return keys registered in the microchip RN2483
        """

        self.exec_serial_cmd("mac get deveui")
        self.exec_serial_cmd("mac get appkey")
        self.exec_serial_cmd("mac get appeui")

    def exec_serial_cmd(self, cmd, get_response=False):
        """
        Executes commands on the serial usb port, and displays the response
        If get_response=True, the function returns the answer
        """

        # execute cmd
        self.ser.write(str.encode(cmd + "\r\n"))
        cmd.rstrip()
        print(cmd)

        time.sleep(0.1)

        # get response
        rdata = self.readline()
        print(rdata)

        if get_response:
            return rdata

    def factory_reset(self):
        """
        reset RN2483 microchip
        """

        self.exec_serial_cmd("sys factoryRESET")

        print("Reset done")

    def config_mac(self):
        """
        configures the RN2483 module according to the commands in the global variable config
        """

        for conf_cmd in config:
            self.exec_serial_cmd(conf_cmd)

    def join(self):
        """
        Performs a "mac join otaa" to "connect" with the gateway
        """

        rdata = self.exec_serial_cmd("mac join otaa", get_response=True)

        if rdata == "ok":

            print("wait for response ...")
            rdata = self.readline()
            print(rdata)

            if rdata != "accepted":
                print("new try...")
                self.join()

    def send_radio(self, data):
        """
        sends data with radio control tx
        """

        rdata = self.exec_serial_cmd("radio tx {}".format(
            binascii.hexlify(data.encode('utf-8')).decode()),
                                     get_response=True)
        if rdata == 'ok':
            rdata = self.readline()
            print(rdata)
        print("wait 5s before sending next message ...")
        time.sleep(5)

    def send(self, data):
        """
        sends data with the command max tx cnf 1
        """

        rdata = self.exec_serial_cmd("mac tx cnf 1 {}".format(
            binascii.hexlify(data.encode('utf-8')).decode()),
                                     get_response=True)
        while rdata != 'ok':
            rdata = self.readline()
            time.sleep(5)
            print(rdata)
            rdata = self.exec_serial_cmd("mac tx cnf 1 {}".format(
                binascii.hexlify(data.encode('utf-8')).decode()),
                                         get_response=True)

        rdata = self.readline()
        time.sleep(5)
        print(rdata)

    def readline(self):
        """
        Returns the serial port responses
        """

        return self.ser.readline()[:-2].decode('UTF-8')
